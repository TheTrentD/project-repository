/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package addserver;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.net.Socket;
import java.net.ServerSocket;
import java.net.InetAddress;

/**
 *
 * @author Austin
 */
public class AddServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {

	    // Create the server socket that will be used to accept
            // incoming connections
            ServerSocket listen = new ServerSocket(2005); // Bind to any port

	    // Print the port so we can run a client that will connect to
            // the server.
            System.out.println("Listening on port:  " + listen.getLocalPort());
            System.out.println("Listening on address: " + InetAddress.getLocalHost());

	    // Process clients forever...
            while (true) {

		// Wait for a client to connect
                Socket client = listen.accept();

                // use the socket to create IO streams
                PrintWriter out = new PrintWriter(client.getOutputStream(), true);

                BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                
                String line = in.readLine();
                if(line.equals("HOLA")){
                    out.println("COMO ESTAS");
                    line = in.readLine();
                    if(line.equals("Help")){
                        out.println("What");
                        line = in.readLine();
                        String[] numbers = line.split(" ");
                        if(numbers[0].equals("Add")){
                            int total = 0;
                            for(int i=1; i<numbers.length; i++){
                                total += Integer.parseInt(numbers[i]);
                            }
                            out.println("Result " + total);
                        }
                        else{
                            out.println("ERROR3");
                        }
                    }else{
                        out.println("ERROR2");
                    }
                }else{
                    out.println("ERROR1");
                }
                
                out.close();
		client.close();
                
            }

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

}
