/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tweetserver;
import java.util.Scanner;
/**
 *
 * @author Trent
 */
public class Driver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ServerDriver d = new ServerDriver();
        int z=0;
        while (z!=1) {
            Scanner menu = new Scanner(System.in);
            System.out.println("Welcome to Server Twitter!");
            System.out.println("1- Register");
            System.out.println("2- Log On");
            System.out.println("3- Log Off");
            System.out.println("4- Follow Someone");
            System.out.println("5- Following Me");
            System.out.println("0- Quit");
            int menuinput = menu.nextInt();
            
            if (menuinput == 1) {
                Scanner newuser = new Scanner(System.in);
                System.out.println("What's The new Username?");
                String newuserinput = newuser.next();
                Scanner newpass = new Scanner(System.in);
                System.out.println("What's The new Password?");
                String newpassinput = newpass.next();
                String result = d.Register(newuserinput, newpassinput);
                System.out.println(result);
            }
            
            if (menuinput == 2) {
               Scanner newuser = new Scanner(System.in);
                System.out.println("Username?");
                String newuserinput = newuser.next();
                Scanner newpass = new Scanner(System.in);
                System.out.println("Password?");
                String newpassinput = newpass.next();
                String result = d.LogOn(newuserinput, newpassinput);
                System.out.println(result);
            }
            
            if (menuinput == 3) {
                Scanner newuser = new Scanner(System.in);
                System.out.println("Username?");
                String newuserinput = newuser.next();
                String result = d.LogOff(newuserinput);
                System.out.println(result);
            }
            
            if (menuinput == 4) {
                Scanner newuser = new Scanner(System.in);
                System.out.println("Username?");
                String newuserinput = newuser.next();
                Scanner newfollow = new Scanner(System.in);
                System.out.println("Follower's Name?");
                String newfollowinput = newfollow.next();
                String result = d.Follow(newuserinput, newfollowinput);
                System.out.println(result);
            }
            
            if (menuinput == 5) {
                Scanner newuser = new Scanner(System.in);
                System.out.println("Username?");
                String newuserinput = newuser.next();
                String result = d.getFollowing(newuserinput);
                System.out.println(result);
            }
            
            if (menuinput == 0) {
                System.out.println("Goodbye");
                z=1;
            }
            if (menuinput > 6) {
                System.out.println("Sorry, you pressed the Wrong Key, Try again \n");
            }
        }
    }
    
}
