/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tweetserver;
import java.util.*;
/**
 *
 * @author Trent
 */
public class Message {
    
    private String body;
    private String hashtag;
    private String username;
    Date difftimestamp = new Date();
    String date = String.format("%tr, %<tD", difftimestamp.getTime());
    
    Message(String u, String b, String h) {
        String currentdate = date; 
        body = b;
        username = u;
        hashtag = h;
    }
    
    public String getBody() {
        return body;
    }
    
    public String getHashtag() {
        return hashtag;
    }
    
    public String getDate() {
        return date;
    }
    
    public String getUsername() {
        return username;
    }

    
}
