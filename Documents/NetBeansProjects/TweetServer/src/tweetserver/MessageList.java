/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tweetserver;

import java.util.HashMap;

/**
 *
 * @author Trent
 */
public class MessageList {
    HashMap<String, TweetList> htl = new HashMap<>();
    TweetList tl = new TweetList();
    
    
    public String addHashtagTweet(String user, String body) {
        String[] b;
        b = body.split("#");
        String nbody =b[0];
        String hash = b[1];
        Message m = new Message (user, nbody, hash);
        boolean n = tl.addTweet(m);
        htl.put(hash, tl);
        String hashtagtweets = tl.getHashtagTweets(hash);
        return hashtagtweets;
    }
    
    public String getHashtagTweet(String hashtag) {
        if(htl.keySet().contains(hashtag)) {
            return tl.getHashtagTweets(hashtag);
        }
        else {
            return null;
        }
    }
}
