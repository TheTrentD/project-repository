/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tweetserver;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.net.Socket;
import java.net.ServerSocket;
import java.net.InetAddress;
import java.util.*;

/**
 *
 * @author Trent
 */
public class OnlineDriver {
    
    
    public static void main(String[] args) throws IOException {
        ServerDriver d = new ServerDriver();
        try {

	    // Create the server socket that will be used to accept
	    // incoming connections

	    ServerSocket listen = new ServerSocket( 2001 ); // Bind to any port

	    // Print the port so we can run a client that will connect to
	    // the server.

	    System.out.println( "Listening on port:  " +
				listen.getLocalPort() );
            System.out.println("Listening on address: " +
                    InetAddress.getLocalHost());

	    // Process clients forever...

	    while ( true ) {
                
		Socket client = listen.accept();

		PrintWriter out =
		    new PrintWriter( client.getOutputStream(), true );

                BufferedReader in =
                new BufferedReader(
                    new InputStreamReader( client.getInputStream() ) );
                
                 
                String line = in.readLine();
                System.out.println(line);
                String[] info = line.split(" ");
                if(info[0].equals("Register")){
                    String user = info[1];
                    String pass = info[2];
                    if (!pass.equals("") && !user.equals("") || user != null && pass != null) {
                        String result = d.Register(user, pass);
                        out.println(result);
                        System.out.println(result);
                    }
                    else {
                        out.print("Decline");
                    }
                 
                    
                }
                if(info[0].equals("LogIn")) {
                    String user = info[1];
                    String pass = info[2];
                    String result = d.LogOn(user, pass);
                    System.out.println(result);
                    out.println(result);
                    String line5 = in.readLine();
                    System.out.println(line5);
                    if(line5.equals("FollowList")) {
                        out.println("Following");
                        String line2 = in.readLine();
                        System.out.println(line2);
                        if(line2.equals("Accept")) {
                            int length;
                            String lengthstring = d.getFollowing(user);
                            if(!lengthstring.equals("-1")) {
                                length = d.getFollowing(user).split(" ").length;
                                out.println("Number " + length);
                                String line3 = in.readLine();
                                System.out.println(line3);
                                if(line3.equals("List")) {
                                    String total = d.getFollowing(user);
                                    String[] stat = total.split(" ");
                                    String ntotal = "";
                                    for (int i=0; i<stat.length; i++) {
                                        System.out.println(stat[i]);
                                        ntotal += stat[i] + "-" +d.checkStatus(stat[i]) + " ";   
                                    }
                                    System.out.println(ntotal);
                                    out.println("FOLLOWING "+ ntotal);

                                    String follower = in.readLine();
                                    if(follower.equals("Followerslist")) {
                                        out.println("Followers");
                                        String follower2 = in.readLine();
                                        System.out.println(follower2);
                                        if(follower2.equals("Accept")) {
                                            int l;
                                            String followerlength = d.getFollowers(user);
                                            if(!followerlength.equals("-1")) {
                                                l = d.getFollowers(user).split(" ").length;
                                                System.out.println(l);
                                                out.println("Number " + l);
                                                String follower3 = in.readLine();
                                                if(follower3.equals("List")) {
                                                    String ftotal = d.getFollowers(user);
                                                    System.out.println(ftotal);
                                                    out.println("FOLLOWER "+ ftotal);
                                                }
                                            }
                                        }
                                    }
                                }
                                else {
                                    out.println("Decline");
                                }
                            }
                            else {
                                out.println("Followerslist");
                                String follower = in.readLine();
                                if(follower.equals("Followerslist")) {
                                        out.println("Followers");
                                        String follower2 = in.readLine();
                                        System.out.println(follower2);
                                        if(follower2.equals("Accept")) {
                                            int l;
                                            String followerlength = d.getFollowers(user);
                                            if(!followerlength.equals("-1")) {
                                                l = d.getFollowers(user).split(" ").length;
                                                System.out.println(l);
                                                out.println("Number " + l);
                                                String follower3 = in.readLine();
                                                if(follower3.equals("List")) {
                                                    String ftotal = d.getFollowers(user);
                                                    System.out.println(ftotal);
                                                    out.println("FOLLOWER "+ ftotal);
                                                }
                                                else {
                                                    out.println("Decline");
                                                }
                                            }
                                            else {
                                                out.println("Decline");
                                            }
                                        }
                                        else {
                                            out.println("Decline");
                                        }
                                    }
                                    else {  
                                        out.println("Decline");
                                    }
                            }
                        }
                        else {
                            out.println("Decline");
                        }
                    }
                    else {
                        out.println("Decline");
                    }
                }
                if(info[0].equals("PublicTweet")) {
                    String user = info[1];
                    out.println("ACCEPT");
                    String bodyresult = in.readLine();
                    System.out.println(bodyresult);
                    String[] bresult = bodyresult.split("-");
                    if (bresult[0].equals("Body")) {
                        String body = bresult[1];
                        if(body != null) {
                            String n;
                            if(!body.endsWith("#") && body.contains("#")) {
                                n = d.addTweet(user, body);
                                if(!n.isEmpty()) {
                                    out.println("Sent Tweets:@" + n);
                                    }
                                else {
                                    out.println("Decline");
                                }
                            }
                            else {
                                out.println("Decline");
                            }
                        }
                        else {
                            out.println("Decline");
                        }
                    }   
                }
                if(info[0].equals("Search")) {
                    String user = info[1];
                    String search = info[2];
                    out.println("Accept"); 
                    String f= d.getHashTag(search);
                    out.println("HashTag List:-" + f);
                }
                
                if(info[0].equals("LogOff")){
                    String user = info[1];
                    String LogOff = d.LogOff(user);
                    out.println(LogOff);
                }
                if(info[0].equals("ViewProfile")){
                    String user = info[1];
                    if(user.contains("-")) {
                        String[] nuser = user.split("-");
                        String newuser= nuser[0];
                        out.println("Accept");
                        String lz = in.readLine();
                        if(lz.equals("List")) {
                            String utweets = d.getProfileTweets(user);
                            if (!utweets.equals("Decline")) {
                                out.println(user+ "'s Tweets:@");
                            }
                            else {
                                out.println("Decline");
                            }
                    }
                        
                        String utweets = d.getProfileTweets(newuser);
                        out.println(utweets);
                    }
                    else {
                        out.println("Accept");
                        String lz = in.readLine();
                        if(lz.equals("List")) {
                            String utweets = d.getProfileTweets(user);
                            if (!utweets.equals("Decline")) {
                                out.println(user+ "'s Tweets:@");
                            }
                            else {
                                out.println("Decline");
                            }
                        }
                    }
                }
                if(info[0].equals("Refresh")) {
                    String user = info[1];
                    String pass = info[2];
                    out.println("Accept");
                    String line5 = in.readLine();
                    System.out.println(line5);
                    if(line5.equals("FollowList")) {
                        out.println("Following");
                        String line2 = in.readLine();
                        System.out.println(line2);
                        if(line2.equals("Accept")) {
                            int length;
                            String lengthstring = d.getFollowing(user);
                            if(!lengthstring.equals("-1")) {
                                length = d.getFollowing(user).split(" ").length;
                                out.println("Number " + length);
                                String line3 = in.readLine();
                                System.out.println(line3);
                                if(line3.equals("List")) {
                                    String total = d.getFollowing(user);
                                    String[] stat = total.split(" ");
                                    String ntotal = "";
                                    for (int i=2; i<stat.length; i++) {
                                        System.out.println(stat[i]);
                                        ntotal += stat[i] + "-" +d.checkStatus(stat[i]) + " ";   
                                    }
                                    
                                    System.out.println(ntotal);
                                    out.println("FOLLOWING "+ ntotal);

                                    String follower = in.readLine();
                                    if(follower.equals("Followerslist")) {
                                        out.println("Followers");
                                        String follower2 = in.readLine();
                                        System.out.println(follower2);
                                        if(follower2.equals("Accept")) {
                                            int l;
                                            String followerlength = d.getFollowers(user);
                                            if(!followerlength.equals("-1")) {
                                                l = d.getFollowers(user).split(" ").length;
                                                System.out.println(l);
                                                out.println("Number " + l);
                                                String follower3 = in.readLine();
                                                if(follower3.equals("List")) {
                                                    String ftotal = d.getFollowers(user);
                                                    System.out.println(ftotal);
                                                    out.println("FOLLOWER "+ ftotal);
                                                }
                                                else {
                                                    out.println("Decline");
                                                    }
                                            }
                                            else {
                                                out.println("Decline");
                                            }
                                        }
                                        else {
                                            out.println("Decline");
                                            }
                                    }
                                    else {
                                        out.println("Decline");
                                        }
                                }
                                else {
                                    out.println("Decline");
                                }
                            }
                            else {
                                out.println("Followerslist");
                                String follower = in.readLine();
                                if(follower.equals("Followerslist")) {
                                        out.println("Followers");
                                        String follower2 = in.readLine();
                                        System.out.println(follower2);
                                        if(follower2.equals("Accept")) {
                                            int l;
                                            String followerlength = d.getFollowers(user);
                                            if(!followerlength.equals("-1")) {
                                                l = d.getFollowers(user).split(" ").length;
                                                System.out.println(l);
                                                out.println("Number " + l);
                                                String follower3 = in.readLine();
                                                if(follower3.equals("List")) {
                                                    String ftotal = d.getFollowers(user);
                                                    System.out.println(ftotal);
                                                    out.println("FOLLOWER "+ ftotal);
                                                }
                                                else {
                                                    out.println("Decline");
                                                }
                                            }
                                            else {
                                                out.println("Decline");
                                            }
                                        }
                                        else {
                                            out.println("Decline");
                                        }
                                    }
                                    else {  
                                        out.println("Decline");
                                    }
                            }
                        }
                        else {
                            out.println("Decline");
                        }
                    }
                    else {
                        out.println("Decline");
                    }
                }
                if(info[0].equals("Follow")) {
                    String user = info[1];
                    String follow = info[2];
                    String followresult = d.Follow(user, follow);
                    System.out.println(d.getFollowing(user));
                    out.println(followresult);
                    System.out.println(followresult);
                    String line5 = in.readLine();
                    System.out.println(line5);
                    if(line5.equals("FollowList")) {
                        out.println("Following");
                        String line2 = in.readLine();
                        System.out.println(line2);
                    
                        if(line2.equals("Accept")) {
                            int length;
                            String lengthstring = d.getFollowing(user);
                            if(!lengthstring.equals("-1")) {
                                length = d.getFollowing(user).split(" ").length;
                                System.out.println(length);
                                out.println("Number " + length);
                                String line3 = in.readLine();
                                System.out.println(line3);
                                if(line3.equals("List")) {
                                    String total = d.getFollowing(user);
                                    String[] stat = line.split(" ");
                                    String ntotal = "";
                                    for (int i=2; i<stat.length; i++) {
                                        System.out.println(stat[i]);
                                        ntotal += stat[i] + "-" +d.checkStatus(stat[i]) + " ";   
                                    }
                                    System.out.println(ntotal);
                                    out.println("FOLLOWING "+ ntotal);
  
                                }
                                if(line.equals("Decline")) {
                                    out.print("Decline3");
                                }
                            }
                        }
                        else {
                            out.println("Decline2");
                        }
                    }
                    else {
                        out.println("Decline1");
                    }
                }
                else {
                    out.println("Decline555");
                }
                out.close();
                client.close();
	    }
            
	}
 catch (IOException e) {
            System.err.println( e.getMessage() );
        }
    }
}
