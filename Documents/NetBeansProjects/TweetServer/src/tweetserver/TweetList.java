/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tweetserver;
import java.util.*;
/**
 *
 * @author Trent
 */
public class TweetList {
    ArrayList<Message> list;

    public TweetList() {
        list = new ArrayList();
    }
    
    public boolean addTweet(Message m) {
        list.add(m);
        return true;
    }

    public String getTweets() {
        String a = "";
        for (int i= 0; i < list.size(); i++) {
            a += list.get(i).getUsername() + ": " + list.get(i).getBody() + " #" + list.get(i).getHashtag() + " " + list.get(i).getDate() + "@";
            System.out.println(a);
        }
        return a;
    }
    
    public String getUserTweet(String user, String body, String hashtag) {
      String a ="";
      for (int i = 0; i<list.size();i++) {
            if(list.get(i).getUsername().equals(user) && list.get(i).getBody().equals(body) && list.get(i).getHashtag().equals(hashtag)) {
                a += list.get(i).getUsername() + ": " + list.get(i).getBody() + " #" + list.get(i).getHashtag() + " " + list.get(i).getDate();
                System.out.println(a);
            }
        }
      return a;
    }
    
    public String getHashtagTweets(String hashtag) {
      String a ="";
      for (int i = 0; i<list.size();i++) {
            if(list.get(i).getHashtag().equals(hashtag)) {
                a += list.get(i).getUsername() + ": " + list.get(i).getBody() + " #" + list.get(i).getHashtag() + " " + list.get(i).getDate()+ "@";
                System.out.println(a);
            }
        }
      return a;
    }
    
    public boolean checkTweets(String user, String body, String hashtag) {
      String a ="";
      for (int i = 0; i<list.size();i++) {
            if(list.get(i).getUsername().equals(user) && list.get(i).getBody().equals(body) && list.get(i).getHashtag().equals(hashtag)) {
                return false;
            }
        }
      return true;
    }
    
    public String getUserTweets(String user) {
      String a ="";
      for (int i = 0; i<list.size();i++) {
            if(list.get(i).getUsername().equals(user)) {
                a += list.get(i).getUsername() + ": " + list.get(i).getBody() + " #" + list.get(i).getHashtag() + " " + list.get(i).getDate()+ "@";
                System.out.println(a);
            }
        }
      return a;
    }
    
}
