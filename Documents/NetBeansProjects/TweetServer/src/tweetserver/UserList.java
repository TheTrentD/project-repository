/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tweetserver;
import java.util.HashMap;
/**
 *
 * @author Trent
 */
public class UserList {
    HashMap<String, User> users = new HashMap <>();
    
    
    public String CheckUsername(String user) {
        if(users.containsKey(user)) {
            User checkeduser = users.get(user);
            String result = checkeduser.GetUser();
            return result;
        }
        else {
           return null; 
        }
    }
    
    public void NewUser(String user, User u) {
        users.put(user, u);
    }
    
    public boolean LogOn(String user) {
      User loguser = users.get(user);
      if (loguser.GetStatus() == false) {
          loguser.LogOnStatus();
          return true;
      }
      else {
          return false;
      }
    }
    
    public String LogOff(String user) {
      User loguser = users.get(user);
      if (loguser.GetStatus() == true) {
          loguser.LogOffStatus();
          return "You are now Logged Off \n";
      }
      else {
          return "You are already Logged Off! \n";
      }
    }
    
    public boolean CheckAccount(String user,String password) {
        if(users.containsKey(user)) {
            User checkeduser = users.get(user);
            return password.equals(checkeduser.GetPassword());
        }
        return false;
    }
    
    public User getUser(String user) {
       return users.get(user);
    }
    
    public String AddFollowers(String user, String follower) {
        if(users.containsKey(user) & users.containsKey(follower)) {
                User checkedfollow = users.get(user);
                String newfollower;
                User checkeduser = users.get(follower);
                newfollower = checkeduser.AddFollower(user);
                String newfollowing = checkedfollow.AddFollowing(follower);
                return "Accept";
        }
        else {
            return "Decline";
        }
    }
    
    public String getFollowingList(String user) {
        if(users.containsKey(user)) {
        User current = users.get(user);
        String list = current.GetFollowings();
        return list;
        }
        else {
            return "Decline";
        }
    }
    
    public String getFollowersList(String user) {
        if(users.containsKey(user)) {
            User current = users.get(user);
            String list = current.GetFollowers();
            return list;
        }
        else {
            return "Decline";
        }
    }
    public String newPublicMessage(String user, String body) {
        User current;
        current = users.get(user);
        String[] b;
        b = body.split("#");
        String nbody =b[0];
        String hash = b[1];
        return current.addPublicTweet(nbody, hash); 
    }
    
    
}
