/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tweetserver;
import java.util.ArrayList;
/**
 *
 * @author Trent
 */
public class User {
    private String username;
    private boolean status;
    private String password;
    private ArrayList<String> Followers= new ArrayList();
    private ArrayList<String> Following= new ArrayList();
    private TweetList tl = new TweetList();
    
    
    public User(String user, String pass) {
      username = user;
      password =  pass;
      status = false;
    }


    
    public String CheckStatus() {
        if (status != true) {
            return "Offline";
        }
        else {
            return "Online";
        }
    }
    
    public boolean GetStatus() {
        return status;
    }
    
    public String GetUser() {
        return username;
    }
    
    public void LogOffStatus() {
        status = false;
    }
    public void LogOnStatus() {
        status = true;
    }
    
    public String GetPassword() {
        return password;
    }
    
    public String GetFollowers() {
        if (Followers.isEmpty()) {
            return "-1";
        }
        else {
            StringBuilder result = new StringBuilder();
            for(int i = 0; i < Followers.size(); i++) {
                result.append(Followers.get(i));
                result.append(" ");
            }
            return result.toString();
        }
    }
    
    public String GetFollower() {
        if (Followers.isEmpty()) {
            return "-1";
        }
        else {
            StringBuilder result = new StringBuilder();
            for(int i = 0; i< Followers.size(); i++) {
                if(username.equals(Followers.get(i))) {
                    result.append(Followers.get(i));
                    result.append(" ");
                }
            }
            return result.toString();
        }
    }
    public String GetFollowings() {
        if (Following.isEmpty()) {
            return "-1";
        }
        else {
            StringBuilder result = new StringBuilder();
            for(int i = 0; i< Following.size(); i++) {
                result.append(Following.get(i));
                result.append(" ");
            }
            return result.toString();
        }
    }
    
    public String GetFollowing() {
        if (Following.isEmpty()) {
            return "-1";
        }
        else {
            StringBuilder result = new StringBuilder();
            for(int i = 0; i< Following.size(); i++) {
                if(username.equals(Following.get(i))) {
                result.append(Following.get(i));
                }
            }
            return result.toString();
        }
    }
    
    public String AddFollower(String follower) {
        Followers.add(follower);
        return follower; 
    }
    
    public String AddFollowing (String user) {
        Following.add(user);
        return user;
    }
    

    public String addPublicTweet(String body, String hashtag) {
        Message m;
        if (tl.checkTweets(username, body, hashtag) == true) {
            m = new Message (username, body, hashtag);
            boolean n;
            n = tl.addTweet(m);
            System.out.println(n);
            String tweetstring;
            tweetstring = tl.getUserTweet(username,body,hashtag);
            return tweetstring;  
        }
        else {
            return "Decline";
        }
    }
    
    public String getUserTweets(String user) {
        String tweetstring = tl.getUserTweets(user);
        return tweetstring;
    }
    
}
