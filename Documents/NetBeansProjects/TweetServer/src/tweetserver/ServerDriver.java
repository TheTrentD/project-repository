/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tweetserver;

import java.util.ArrayList;
import java.util.LinkedList;




/**
 *
 * @author Trent
 */
public class ServerDriver {
    UserList ul = new UserList();
    MessageList ml = new MessageList();
    
    public String Register(String user, String password) {
        if(user.equals(ul.CheckUsername(user))) {
            return "Decline";
        }
        else {
            User newuser = new User(user, password);
            ul.NewUser(user, newuser);
            return "Accept";
        } 
    }
    
    public String LogOn(String user, String password) {
        if(user.equals(ul.CheckUsername(user))) {
            if(ul.CheckAccount(user, password) == true) {
                boolean logonuser = ul.LogOn(user);
                if(logonuser = true) {
                    return "Accept";
                }
                else {
                    return "Decline1";
                }
            }
            else {
              return "Decline2";  
            
        }
      }
        else {
            return "Decline3";
        }
    }
    
    public String LogOff(String user) {
        if(user.equals(ul.CheckUsername(user))) {
            ul.LogOff(user);
            return "Accept";
    }
        else
            return "Decline";
        
    }
    
    public String Follow(String user, String follower) {
        User current = ul.getUser(user);
        if (user.equals(ul.CheckUsername(user))) {
            if(!ul.getFollowingList(user).contains(follower)) {
                if (current.GetStatus()== true) {
                    String follow = ul.AddFollowers(user, follower);
                    return follow;  
                }
                else {
                    return "Decline";
                }
            }
            else {
                return "Special";
            }
        }
        else {
            return "Decline";
        }
    }
    
    public String getFollowing(String user) {
        String result = ul.getFollowingList(user);
        return result;
    }
    public String getFollowers(String user) {
       String result = ul.getFollowersList(user);
       return result;
    }
    
    
    public String addTweet(String user, String body) {
        if (user.equals(ul.getUser(user).GetUser())) {
                String u = ul.newPublicMessage(user, body);
                String ht = ml.addHashtagTweet(user, body);
                System.out.println(ht);
                if (!u.isEmpty() && !ht.isEmpty()) {
                    return u;
                }
                else {
                    return null;
                }
        }
        else {
            return null;
        }
    }
    
    public String getHashTag(String hashtag) {
        if(hashtag.startsWith("#", 0)) {
           String[] newh = hashtag.split("#");
           String newhashtag = newh[1];
           String hashtaglist = ml.getHashtagTweet(newhashtag);
            if(hashtaglist != null) {
                return hashtaglist;
            }
            else {
                return "Wrong Hashtag! Try again.";
            }
        }
        String hashtaglist = ml.getHashtagTweet(hashtag);
        if(hashtaglist != null) {
            return hashtaglist;
        }
        else {
            return "Wrong Hashtag! Try again.";
        }
    }
    
    public String checkStatus(String user) {
        return ul.getUser(user).CheckStatus();
    }
    
    public String getProfileTweets(String user) {
        if(ul.getUser(user).GetUser().equals(user)) {
            return ul.getUser(user).getUserTweets(user);
        }
        else {
            return "Decline";
        }
    }
}
